/**
 * @file cIdleBehaviour.cpp
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * Implementation of the cIdleBehaviour class.
 */

#include "cIdleBehaviour.h"

cIdleBehaviour::cIdleBehaviour(cEntity* agent, float timeToWait) {

	this->mAgent = agent;
	this->mTimeToWait = timeToWait;
	this->mTotalTime = 0.0f;

	return;
}

cIdleBehaviour::~cIdleBehaviour() {

	return;
}

void cIdleBehaviour::update(cEntity* pEntity, float deltaTime) {

	// Check if the behaviour is finished
	if (this->isFinished()) return;

	// Otherwise add to the total time 
	this->mTotalTime += deltaTime;

	return;
}

bool cIdleBehaviour::isFinished() {
	
	return this->mTotalTime >= this->mTimeToWait;
}

void cIdleBehaviour::startBehaviour()
{
}

void cIdleBehaviour::stopBehaviour()
{
}

bool cIdleBehaviour::isStarted()
{
	return false;
}

void cIdleBehaviour::refreshIdle() {

	this->mTotalTime = 0.0f;

	return;
}
