#include "cSkinnedMeshComponent.h"

#include "sModelDrawInfo.h"
#include "cVAOMeshManager.h"
#include "cShaderManager.h"
#include "cMesh.h"

// c'tor
cSkinnedMeshComponent::cSkinnedMeshComponent() : cComponent(10) {

	this->pSkinnedMesh = new cSimpleAssimpSkinnedMesh();
	this->pAniState = nullptr;

	return;
}