#include <Global.h>
#include <cVAOMeshManager.h>
#include <cEntityBuilder.h>
#include <cSceneManager.h>
#include <cLightManager.h>
#include <cSoundManager.h>
#include <cLuaBrain.h>
#include <cScriptingCommandSystem.h>
#include <cColliderSystem.h>
#include <cSerialization.h>
#include <cFrameBufferComponent.h>
#include <cThreadComponent.h>
#include <fstream>

#include <cBehaviourSystem.h>
#include <cBehaviourComponent.h>
#include <cFormationBehaviour.h>
#include <cCrowdFollowBehaviour.h>
#include <cFlockBehaviour.h>

cEntity* pSphere = nullptr;
cEntity* pCube = nullptr;

sLight* pMovingSpotLight = nullptr;

extern std::map<std::string, cCommandGroup*> commandGroups;
extern std::map<std::string, iCommand*> commands;
extern cAABBTerrianBroadPhaseHierarchy g_AABBTerrianHierarchy;
cFormationBehaviour* pFormation;
cCrowdFollowBehaviour* pCrowdFollow;

void set_rand_position(cTransformComponent* transform_component)
{
	float x = rand() % 200 - 100.0f;
	float z = rand() % 200 - 100.0f;

	transform_component->setPosition(glm::vec3(x, 0.0f, z));
}

void loadScene() {

	// create a scene for the entities
	cSceneManager* pSceneManager = cSceneManager::getInstance();
	cScene* pSandboxScene = pSceneManager->createScene("sandbox.json");
	pSceneManager->setActiveScene("sandbox.json");

	//initialize the light manager before loading them
	cLightManager::getInstance()->initializeLights();

	pSphere = cEntityBuilder::getInstance()->createEntity(0);
	cRenderMeshComponent* sphereMesh = pSphere->getComponent<cRenderMeshComponent>();
	sphereMesh->bIsVisible = false;
	sphereMesh->friendlyName = "Sphere";
	sphereMesh->bIsWireFrame = false;


	pCube = cEntityBuilder::getInstance()->createEntity(3);
	cRenderMeshComponent* cubeMesh = pCube->getComponent<cRenderMeshComponent>();
	cubeMesh->bIsVisible = false;
	cubeMesh->friendlyName = "Cube";
	cubeMesh->bIsWireFrame = false;

	cColliderSystem::getInstance()->pColliderSphere = pSphere;
	cColliderSystem::getInstance()->pAABBCube = pCube;

	//load from the file
	cSerialization::deserializeSceneCamera("cameras.json");
	cSerialization::deserializeSceneLights("lights.json");
	cSerialization::deserializeSceneSounds("sounds.json");
	cSerialization::deserializeSceneEntities("entities.json");

	cEntityBuilder* pBuilder = cEntityBuilder::getInstance();

	// Create a path for the crowd to follow
	sPath path;
	path.pathNodes.push_back(sPathNode(glm::vec3(-200.0f, 0.0f, -60.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(-12.0f, 0.0f, -177.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(-180.0f, 0.0f, 189.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(-2.0f, 0.0f, 139.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(180.0f, 0.0f, -174.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(200.0f, 0.0f, 156.0f)));
	path.pathNodes.push_back(sPathNode(glm::vec3(-190.0f, 0.0f, -195.0f)));

	// Create formation
	pFormation = new cFormationBehaviour();
	pFormation->setFormation(cFormationBehaviour::NONE);
	pFormation->setCrowdNucleus(glm::vec3(0.0f));

	// Create Crowd follow behaviour
	pCrowdFollow = new cCrowdFollowBehaviour(pFormation);
	pCrowdFollow->setPath(path);
	pCrowdFollow->startBehaviour();
	cBehaviourSystem::getInstance()->registerCrowdBehaviour(pCrowdFollow);

	// Add entities to the crowd / Scene
	std::vector<cEntity*> boids;
	for (size_t index = 0; index < 12; ++index) {

		cEntity* pDalek = pBuilder->createEntity(7);
		cTransformComponent* pDalekTrans = pDalek->getComponent<cTransformComponent>();

		set_rand_position(pDalekTrans);

		pFormation->registerEntity(pDalek);
		boids.push_back(pDalek);
		pSandboxScene->addEntityToScene(pDalek);
	}

	// Create a Flocking behaviour for each boid
	for (size_t index = 0; index < boids.size(); ++index) {

		cBehaviourComponent* behaviourComp = boids[index]->getComponent<cBehaviourComponent>();
		cFlockBehaviour* flock = behaviourComp->addBehaviour<cFlockBehaviour>();

		// Set the weights for the flock
		flock->flockCenter = glm::vec3(0.0f, 0.0f, 0.0f);
		flock->cohesionWeight = 0.3f;
		flock->separationWeight = 0.3f;
		flock->alignmentWeight = 0.3f;

		// Add the other boids to the flock for each boid (local flock)
		for (size_t j = 0; j < boids.size(); ++j) {

			// Check if the current boid is the owner of this flock behaviour
			if (boids[index] == boids[j]) continue;

			// Now create a sBoid and add it to the flock
			cFlockBehaviour::sBoid boid;
			boid.entity = boids[j];
			boid.transform = boids[j]->getComponent<cTransformComponent>();

			flock->addBoid(boid);
		}

		// Add the flock to the system
		cBehaviourSystem::getInstance()->registerEntity(boids[index]);
	}

	return;
}