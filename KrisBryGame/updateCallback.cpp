#include <cEngine.h>
#include <cConsole.h>
#include <cDebugRenderer.h>

#include <cSceneManager.h>
#include <cShaderManager.h>
#include <cFreeTypeRenderer.h>

#include <cTransformComponent.h>
#include <cRenderMeshComponent.h>
#include <cBehaviourComponent.h>
#include <cFollowBehaviour.h>
#include <cCrowdFollowBehaviour.h>
#include <cFlockBehaviour.h>

#include <cMeshRenderSystem.h>
#include <cBehaviourSystem.h>

#include <physicsShapes.h>
#include <cColliderSystem.h>

#include <iInputCommand.h>
#include <cDebugUtilities.h>


extern cEntity* pSphere;
extern cEntity* pCube;

extern cAABBTerrianBroadPhaseHierarchy g_AABBTerrianHierarchy;
extern cCrowdFollowBehaviour* pCrowdFollow;

double nextLightSwitchTime = 0;

//void showModelNormals();
void checkFlockFollowPath();

float randomFloat(float a, float b) {
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

void updateCallback(double deltaTime) {

	cShaderManager::getInstance()->useShaderProgram("BasicUberShader");
	GLint program = cShaderManager::getInstance()->getIDFromFriendlyName("BasicUberShader");

	cSceneManager* pSceneManager = cSceneManager::getInstance();
	cScene* pScene = pSceneManager->getActiveScene();

	// Follow the selected entity
	cEntity* pSelectedEntity = pScene->getSelectedEntity();
	//pScene->flyCamera.followSelectedEntity(deltaTime, pSelectedEntity); // Animation Midterm Q5

	glm::vec3 center = pSelectedEntity->getComponent<cBehaviourComponent>()->getBehaviour<cFlockBehaviour>()->flockCenter;
	std::cout << "Flock Center: x: " << center.x << " y: " << center.y << " z: " << center.z << std::endl;

	// Debug render
	if (pScene->bIsLightDebug) {
		pScene->setLightDebugSphere(pSphere);
	}

	if (pScene->bDisplayCamInfo) {
		
		cDebugUtilities::renderCameraInfo();
	}

	// Show the checkpoints as the entities gat close to it
	sPath path = pCrowdFollow->getPath();
	int currCheckpoint = pCrowdFollow->getCurrentCheckpoint();
	for (size_t index = 0; index < path.pathNodes.size(); ++index) {

		// Get the needed components of the cube to render
		cTransformComponent* checkpointTrans = pCube->getComponent<cTransformComponent>();
		cRenderMeshComponent* checkpointMesh = pCube->getComponent<cRenderMeshComponent>();

		// Set the position of the cube to where the checkpoint is
		checkpointTrans->setPosition(path.pathNodes[index].position);
		checkpointMesh->bIsVisible = true;
		checkpointMesh->bIsWireFrame = false;

		// Set the colour to red if its not the currently seeked checkpoint
		if (currCheckpoint != index) {

			checkpointMesh->setDiffuseColour(glm::vec3(1.0f, 0.0f, 0.0f));
		}
		else { // Green

			checkpointMesh->setDiffuseColour(glm::vec3(0.0f, 1.0f, 0.0f));
		}

		// Draw it
		glm::mat4 matModel = glm::mat4(1.0f);
		cMeshRenderSystem::getInstance()->drawObject(pCube, matModel);
		checkpointMesh->bIsVisible = false;
	}

	// Flocking values
	cDebugUtilities::renderFlockInfo();

	if (pCrowdFollow->mBIsFollowingPath) {
		checkFlockFollowPath();
	}

}