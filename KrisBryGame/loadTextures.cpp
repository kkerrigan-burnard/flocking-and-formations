#include <cBasicTextureManager.h>
#include <iostream>

void loadTextures() {
	cBasicTextureManager* pBasicTextureManager = cBasicTextureManager::getInstance();
	pBasicTextureManager->SetBasePath("./assets/textures/");

	
	if (!pBasicTextureManager->Create2DTextureFromBMPFile("wood.bmp", true)) {
		std::cout << "Did not load wood.bmp" << std::endl;
	}

	if (!pBasicTextureManager->Create2DTextureFromBMPFile("debug_grid.bmp", true)) {
		std::cout << "Did not load debug_grid.bmp" << std::endl;
	}

	// Animation Midterm Q1
	if (!pBasicTextureManager->Create2DTextureFromBMPFile("gravel.bmp", true)) {
		std::cout << "Did not load gravel.bmp.bmp" << std::endl;
	}

	if (!pBasicTextureManager->Create2DTextureFromBMPFile("rock.bmp", true)) {
		std::cout << "Did not load rock.bmp" << std::endl;
	}

	// Load cube maps
	pBasicTextureManager->SetBasePath("assets/textures/cubemaps");
	std::string errorString;

	if (pBasicTextureManager->CreateCubeTextureFromBMPFiles("TropicalDayCubeMap",
		"SpaceBox_right1_posX.bmp", "SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp", "SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp", "SpaceBox_back6_negZ.bmp", true, errorString))
	{
		std::cout << "Loaded the Space cube map OK" << std::endl;
	}
	else {
		std::cout << "Error: sunny day cube map DIDN't load. On no!" << std::endl;
	}

	return;
}