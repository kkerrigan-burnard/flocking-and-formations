/**
 * @file
 * @author  Kristian Kerrigan <k_kerrigan3@fanshaweonline.ca>
 *
 * @section DESCRIPTION
 *
 * Enter a short description.
 */

#include <iostream>

#include <physics/interfaces/iSphereShape.h>
#include <physics/interfaces/iPlaneShape.h>

#include <gameMath.h>

#include "cPhysicsWorld.h"
#include <algorithm> // for deleting

nPhysics::cPhysicsWorld::~cPhysicsWorld() {

	return;
}

void nPhysics::cPhysicsWorld::setGravity(const glm::vec3& grav) {

	
	return;
}

bool nPhysics::cPhysicsWorld::addRigidBody(iRigidBody* pBody) {
	
	// TODO: Check if it already exists
	mBodies.push_back(pBody);
	
	return true;
}

bool nPhysics::cPhysicsWorld::removeRigidBody(iRigidBody* pBody) {
	
	// TODO: do proper vector delete
	
	return false;
}

void nPhysics::cPhysicsWorld::update(float deltaTime) {

	this->mElaspedTime += deltaTime;

	// STEP 1: Intergrate RK4
	size_t numBodies = mBodies.size();
	for (size_t index = 0; index < numBodies; ++index) {

		nPhysics::iRigidBody* currBody = this->mBodies[index];

		// Integrate spheres
		if (currBody->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_SPHERE) {

			// get current position / velocity and store in State object
			glm::vec3 curPos;
			glm::vec3 curVel;

			currBody->getPosition(curPos);
			currBody->getVelocity(curVel);
			
			State currState;
			currState.position = curPos;
			currState.velocity = curVel;

			//currBody->applyFoce(glm::vec3(0.0f, -5.0f, 0.0f));
			
			glm::vec3 curAccel;
			currBody->getAcceleration(curAccel);

			this->integrate(currState, curAccel, deltaTime);

			currBody->setVelocity(currState.velocity);
			currBody->setPosition(currState.position);

			// set acceleration to zero at the end of the frame
			currBody->setAcceleration(glm::vec3(0.0f));
		}

		// store the last position
		glm::vec3 lastPos;
		currBody->getPosition(lastPos);
		currBody->setPreviousPosition(lastPos);
	}

	// STEP 2: Collision detection
	std::vector<nPhysics::sCollisionDetails> collisions;
	for (size_t idxA = 0; idxA < numBodies - 1; ++idxA) {
		for (size_t idxB = idxA + 1; idxB < numBodies; ++idxB) {

			nPhysics::sCollisionDetails currCollision;

			if (this->isColliding(mBodies[idxA], mBodies[idxB], currCollision, deltaTime)) {

				collisions.push_back(currCollision);
			}
		}
	}

	// STEP 3: Collision Response
	size_t numCollisions = collisions.size();
	for (size_t index = 0; index < numCollisions; ++index) {

		nPhysics::sCollisionDetails currCollision = collisions[index];

		if (currCollision.collisionType == nPhysics::sCollisionDetails::eCollisionType::UNKNOWN) {

			std::cout << "Unknown collision stored at index " << index << std::endl;
			continue;
		}

		// Sphere-Plane collisions
		if (currCollision.collisionType == nPhysics::sCollisionDetails::eCollisionType::SPHERE_PLANE) {

			this->spherePlaneCollisionResponse(currCollision, deltaTime);
		}

		// sphere-sphere collisions
		if (currCollision.collisionType == nPhysics::sCollisionDetails::eCollisionType::SPHERE_SPHERE) {

			this->sphereSphereCollisionResponse(currCollision, deltaTime);
		}
	}

	return;
}

bool nPhysics::cPhysicsWorld::isColliding(iRigidBody* bodyA, iRigidBody* bodyB, nPhysics::sCollisionDetails& collision, float deltaTime) {
	
	float timestep;
	glm::vec3 q;

	if (bodyA->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_SPHERE) {

		// Sphere-Plane collisions
		if (bodyB->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_PLANE) {

			glm::vec3 currSpherePos;
			bodyA->getPosition(currSpherePos);

			glm::vec3 currSpherePrevPos;
			bodyA->getPreviousPosition(currSpherePrevPos);

			glm::vec3 currSphereVel;
			bodyA->getVelocity(currSphereVel);

			int result = this->intersectMovingSpherePlane(bodyA, (currSpherePos - currSpherePrevPos), bodyB, timestep, q);

			if (result == 1) {

				if (timestep > 0.0f && timestep < 1.0f) {

					glm::vec3 planeVel;
					bodyB->getVelocity(planeVel);

					collision.collisionType = nPhysics::sCollisionDetails::eCollisionType::SPHERE_PLANE;
					collision.contactPosition = q;
					collision.bodyA = bodyA;
					collision.bodyB = bodyB;
					collision.contactVelocityA = currSphereVel;
					collision.contactVelocityB = planeVel;
					collision.contactNormal = static_cast<iPlaneShape*>(bodyB->getShape())->getPlaneNormal();
					collision.timeStep = timestep;

					return true;
				}
			}

			if (result == -1) {

				// Set the sphere to its previous position and project it off the normal
				glm::vec3 normal = static_cast<iPlaneShape*>(bodyB->getShape())->getPlaneNormal();

				glm::vec3 newSpherePos;
				//bodyA->getPreviousPosition(newSpherePos);
				newSpherePos = currSpherePos + (normal * deltaTime);
				newSpherePos.y += static_cast<iSphereShape*>(bodyA->getShape())->getRadius();
				bodyA->setPosition(newSpherePos);
			}
		}

		// Sphere-Sphere collisions
		if (bodyB->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_SPHERE) {

			int result = this->intersectMovingSphereSphere(bodyA, bodyB, timestep);

			if (result == 1) {

				if (timestep > 0.0f && timestep < 1.0f) {
					
					glm::vec3 currSpherePos;
					bodyA->getPosition(currSpherePos);

					glm::vec3 collideSpherePos;
					bodyB->getPosition(collideSpherePos);

					glm::vec3 currSphereVel;
					bodyA->getVelocity(currSphereVel);

					glm::vec3 collideSphereVel;
					bodyB->getVelocity(collideSphereVel);

					collision.collisionType = nPhysics::sCollisionDetails::eCollisionType::SPHERE_SPHERE;
					collision.bodyA = bodyA;
					collision.bodyB = bodyB;
					collision.contactVelocityA = currSphereVel;
					collision.contactVelocityB = collideSphereVel;
					collision.contactNormal = glm::normalize(collideSpherePos - currSpherePos); //currSpherePos - collideSpherePos
					collision.timeStep = timestep; 

					return true;
				}

				if (result == -1) {

					glm::vec3 bodyAPrevPos;
					glm::vec3 bodyBPrevPos;

					bodyA->getPreviousPosition(bodyAPrevPos);
					bodyB->getPreviousPosition(bodyBPrevPos);

					bodyA->setPosition(bodyAPrevPos);
					bodyB->setPosition(bodyBPrevPos);
				}
			}
		}
	}

	if (bodyB->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_SPHERE) {

		// Sphere-Plane collisions
		if (bodyA->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_PLANE) {

			glm::vec3 curSpherePos;
			bodyB->getPosition(curSpherePos);

			glm::vec3 curSpherePrevPos;
			bodyB->getPreviousPosition(curSpherePrevPos);

			glm::vec3 currSphereVel;
			bodyB->getVelocity(currSphereVel);

			glm::vec3 curSphereVel;
			bodyB->getVelocity(curSphereVel);

			int result = this->intersectMovingSpherePlane(bodyB, (curSpherePos - curSpherePrevPos), bodyA, timestep, q);

			if (result == 1) {

				if (timestep > 0.0f && timestep < 1.0f) {

					glm::vec3 planeVel;
					bodyA->getVelocity(planeVel);

					collision.collisionType = nPhysics::sCollisionDetails::eCollisionType::SPHERE_PLANE;
					collision.contactPosition = q;
					collision.bodyA = bodyB;
					collision.bodyB = bodyA;
					collision.contactVelocityA = curSphereVel;
					collision.contactVelocityB = planeVel;
					collision.contactNormal = static_cast<iPlaneShape*>(bodyA->getShape())->getPlaneNormal();
					collision.timeStep = timestep;

					return true;
				}
			}

			if (result == -1) {

				// Set the sphere to its previous position and project it off the normal
				glm::vec3 normal = static_cast<iPlaneShape*>(bodyA->getShape())->getPlaneNormal();

				glm::vec3 newSpherePos;
				//bodyB->getPreviousPosition(newSpherePos);
				newSpherePos = curSpherePos + (normal * deltaTime);
				newSpherePos.y += static_cast<iSphereShape*>(bodyB->getShape())->getRadius();
				bodyB->setPosition(newSpherePos);
			}
		}

		// Sphere-Sphere collisions
		if (bodyA->getShape()->getShapeType() == eShapeType::SHAPE_TYPE_SPHERE) {

			int result = this->intersectMovingSphereSphere(bodyB, bodyA, timestep);

			if (result == 1) {

				if (timestep > 0.0f && timestep < 1.0f) {

					glm::vec3 currSpherePos;
					bodyB->getPosition(currSpherePos);

					glm::vec3 collideSpherePos;
					bodyA->getPosition(collideSpherePos);

					glm::vec3 currSphereVel;
					bodyB->getVelocity(currSphereVel);

					glm::vec3 collideSphereVel;
					bodyA->getVelocity(collideSphereVel);

					collision.collisionType = nPhysics::sCollisionDetails::eCollisionType::SPHERE_SPHERE;
					collision.bodyA = bodyB;
					collision.bodyB = bodyA;
					collision.contactVelocityA = currSphereVel;
					collision.contactVelocityB = collideSphereVel;
					collision.contactNormal = glm::normalize(collideSpherePos - currSpherePos); //currSpherePos - collideSpherePos
					collision.timeStep = timestep;

					return true;
				}
			}

			if (result == -1) {

				glm::vec3 bodyAPrevPos;
				glm::vec3 bodyBPrevPos;

				bodyA->getPreviousPosition(bodyAPrevPos);
				bodyB->getPreviousPosition(bodyBPrevPos);

				bodyA->setPosition(bodyAPrevPos);
				bodyB->setPosition(bodyBPrevPos);
			}
		}
	}
	
	return false;
}

int nPhysics::cPhysicsWorld::intersectMovingSpherePlane(iRigidBody* sphere, glm::vec3 movementVector, iRigidBody* plane, float& timestep, glm::vec3& q) const {
	
	// Get the shapes
	nPhysics::iSphereShape* sphereShape = static_cast<nPhysics::iSphereShape*>(sphere->getShape());
	nPhysics::iPlaneShape* planeShape = static_cast<nPhysics::iPlaneShape*>(plane->getShape());

	// Compute distance of sphere center to plane
	glm::vec3 spherePos;
	glm::vec3 planePos;
	sphere->getPosition(spherePos);
	plane->getPosition(planePos);
	float distance = glm::dot(spherePos - planePos, planeShape->getPlaneNormal());
	if (glm::abs(distance) <= sphereShape->getRadius()) {
		// The sphere is already overlapping the plane
		timestep = 0.0f;
		q = spherePos;
		return -1;
	}

	glm::vec3 sphereVel;
	sphere->getVelocity(sphereVel);
	float denom = glm::dot(planeShape->getPlaneNormal(), glm::normalize(sphereVel));
	if (denom * distance >= 0.0f) {
		// No intersection
		return 0;
	}
	else {
		// Sphere is moving towards the plane
		// Use +radius in computations if sphere in front of plane, else -radius
		float radius = distance > 0.0f ? sphereShape->getRadius() : -sphereShape->getRadius();
		timestep = (radius - distance) / denom;
		q = spherePos + timestep * sphereVel - radius * planeShape->getPlaneNormal();
		return 1;
	}
}

int nPhysics::cPhysicsWorld::intersectMovingSphereSphere(iRigidBody* sphereA, iRigidBody* sphereB, float& timestep) const {
	
	// Get sphere shapes
	iSphereShape* sphereShapeA = static_cast<iSphereShape*>(sphereA->getShape());
	iSphereShape* sphereShapeB = static_cast<iSphereShape*>(sphereB->getShape());

	// Get sphere positions
	glm::vec3 sphereAPos;
	glm::vec3 sphereBPos;
	sphereA->getPosition(sphereAPos);
	sphereB->getPosition(sphereBPos);

	// Get sphere velocities
	glm::vec3 sphereAVel;
	glm::vec3 sphereBVel;
	sphereA->getVelocity(sphereAVel);
	sphereB->getVelocity(sphereBVel);

	glm::vec3 s = sphereBPos - sphereAPos;
	glm::vec3 v = glm::normalize(sphereBVel - sphereAVel);
	float r = sphereShapeA->getRadius() + sphereShapeB->getRadius();
	float c = glm::dot(s, s) - r * r;

	if (c < 0.0f) {
		// spheres are already intersecting
		timestep = 0.0f;
		return -1;
	}

	float a = glm::dot(v, v);
	if (a < glm::epsilon<float>()) {
		// not moving relative to each other
		return 0;
	}

	float b = glm::dot(v, s);
	if (b >= 0.0f) {
		// moving away from each other
		return 0;
	}

	float d = b * b - a * c;
	if (d < 0.0f) {
		// no intersection
		return 0;
	}

	timestep = (-b - glm::sqrt(d)) / a;
	return 1;
}

nPhysics::Derivative nPhysics::cPhysicsWorld::evaluate(const State& initialState, glm::vec3 t, float deltaTime, const Derivative& d) {
	
	State state;
	state.position = initialState.position + d.dx * deltaTime;
	state.velocity = initialState.velocity + d.dv * deltaTime;

	Derivative output;
	output.dx = state.velocity;
	output.dv += t;

	return output;
}

void nPhysics::cPhysicsWorld::integrate(State& state, glm::vec3 t, float deltaTime) {

	Derivative a, b, c, d;

	a = evaluate(state, t, 0.0f, Derivative());
	b = evaluate(state, t, deltaTime * 0.5f, a);
	c = evaluate(state, t, deltaTime * 0.5f, b);
	d = evaluate(state, t, deltaTime, c);

	glm::vec3 dxdt = 1.0f / 6.0f *	(a.dx + 2.0f * (b.dx + c.dx) + d.dx);

	glm::vec3 dvdt = 1.0f / 6.0f * (a.dv + 2.0f * (b.dv + c.dv) + d.dv);

	state.position = state.position + dxdt * deltaTime;
	state.velocity = state.velocity + dvdt * deltaTime;

	return;
}

void nPhysics::cPhysicsWorld::spherePlaneCollisionResponse(sCollisionDetails& collision, float deltaTime) {

	// TODO: Make drop off value configurable
	float dropOff = 0.15f; // velocity decay

	float timeWithoutCollision = (deltaTime * collision.timeStep);
	float timeAfterCollision = deltaTime - timeWithoutCollision;

	// reflect velocity off the plane normal
	glm::vec3 reflectVec = glm::reflect(collision.contactVelocityA, collision.contactNormal) * (1.0f - dropOff);

	collision.bodyA->setVelocity(reflectVec);

	// Continue the time step
	glm::vec3 newPos;
	collision.bodyA->getPosition(newPos);

	glm::vec3 newVel;
	collision.bodyA->getVelocity(newVel);

	State state;
	state.position = newPos;
	state.velocity = newVel;
	this->integrate(state, glm::vec3(0.0f), timeAfterCollision);
	collision.bodyA->setVelocity(state.velocity);
	collision.bodyA->setPosition(state.position);

	return;
}

void nPhysics::cPhysicsWorld::sphereSphereCollisionResponse(sCollisionDetails& collision, float deltaTime) {

	float dropOff = 0.15f;

	float timeWithoutCollision = (deltaTime * collision.timeStep);
	float timeAfterCollision = deltaTime - timeWithoutCollision;

	glm::vec3 bodyACurPos;
	collision.bodyA->getPosition(bodyACurPos);

	glm::vec3 bodyBCurPos;
	collision.bodyB->getPosition(bodyBCurPos);

	glm::vec3 bodyAVel;
	collision.bodyA->getVelocity(bodyAVel);

	glm::vec3 bodyBVel;
	collision.bodyB->getVelocity(bodyBVel);

	// move the spheres back to where they collided
	glm::vec3 bodyAContact = bodyACurPos - (bodyAVel * timeAfterCollision);
	glm::vec3 bodyBContact = bodyBCurPos - (bodyBVel * timeAfterCollision);
	collision.bodyA->setPosition(bodyAContact);
	collision.bodyB->setPosition(bodyBContact);

	// Do inelastic collisions
	float bodyAMass;
	float bodyBMass;

	collision.bodyA->getMass(bodyAMass);
	collision.bodyB->getMass(bodyBMass);
	glm::vec3 bodyAInitVel = collision.contactVelocityA * (1.0f - dropOff);
	glm::vec3 bodyBInitVel = collision.contactVelocityB * (1.0f - dropOff);

	// Use formula found here to calculate new velocity: https://en.wikipedia.org/wiki/Inelastic_collision
	// TODO: Make  coefficient of restitution configurable
	float coefficient = 0.45f;
	glm::vec3 newVelA = ((((bodyAMass - bodyBMass) / (bodyAMass + bodyBMass)) * bodyAInitVel) + (((2 * bodyBMass) / (bodyAMass + bodyBMass)) * bodyBInitVel) * coefficient);
	glm::vec3 newVelB = ((((-bodyAMass + bodyBMass) / (bodyAMass + bodyBMass)) * bodyBInitVel) + (((2 * bodyAMass) / (bodyAMass + bodyBMass)) * bodyAInitVel) * coefficient);

	newVelA += 0.38f * collision.contactNormal * glm::dot(newVelB, collision.contactNormal);
	newVelB += 0.38f * collision.contactNormal * glm::dot(newVelA, collision.contactNormal);

	// Set the new velocities
	collision.bodyA->setVelocity(newVelA);
	collision.bodyB->setVelocity(newVelB);

	// Continue the time step
	glm::vec3 bodyAPos;
	glm::vec3 bodyBPos;

	collision.bodyA->getPosition(bodyAPos);
	collision.bodyB->getPosition(bodyBPos);

	State newStateA;
	newStateA.position = bodyAPos;
	newStateA.velocity = newVelA;

	this->integrate(newStateA, glm::vec3(0.0f), timeAfterCollision);
	collision.bodyA->setVelocity(newStateA.velocity);
	collision.bodyA->setPosition(newStateA.position);

	State newStateB;
	newStateB.position = bodyBPos;
	newStateB.velocity = newVelB;

	this->integrate(newStateB, glm::vec3(0.0f), timeAfterCollision);
	collision.bodyB->setVelocity(newStateB.velocity);
	collision.bodyB->setPosition(newStateB.position);

	return;
}
